"use strict";

class FilterDirective {
    controller = "FilterController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/filter/filter.html";
    bindToController = true;
    scope = {
        filter: "="
    };
}

angular.module("app")
    .directive("filter", FilterDirective);

