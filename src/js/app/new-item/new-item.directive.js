"use strict";

class NewItemDirective {
    controller = "NewItemController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/new-item/new-item.html";
    bindToController = true;
    scope = {
    };
}

angular.module("app").directive("newItem", NewItemDirective);
