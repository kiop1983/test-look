"use strict";

class NewItemController {
    title;

    constructor(eventService, dataService) {
        this.eventService = eventService;
        this.dataService = dataService;
    }

    add() {
        if(!this.title) { return; }

        const item = {
            date: Date.now(),
            title: this.title,
            tags: [],
            id: this.dataService.makeDataId()
        };

        this.eventService.fireAddItem(item);
    }
};

NewItemController.$inject = ["EventService", "DataService"];

angular.module("app").controller("NewItemController", NewItemController);
