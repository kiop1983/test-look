"use strict";

class SidebarController {
    selectedItem;
    newTag;
    unsubscriveSelectItem;

    constructor(eventService) {
        this.eventService = eventService;
    }

    $onInit() {
        this.unsubscriveSelectItem = this.eventService.subscribeSelectItem((selectedItem) => {
            this.selectedItem = selectedItem;
        });
    }

    removeTag(tag) {
        let index = this.selectedItem.tags.indexOf(tag);
        this.selectedItem.tags.splice(index, 1);
        this.eventService.fireChangeTag(this.selectedItem);
    }

    addTag() {
        if (!this.newTag) { return; }
        if (!this.selectedItem.tags) { this.selectedItem.tags = []; }
        const lower = this.newTag.toLowerCase();
        const index = this.selectedItem.tags.map((t) => t.toLowerCase()).indexOf(lower);
        if (index === -1) {
            this.selectedItem.tags.push(this.newTag);
            this.eventService.fireChangeTag(this.selectedItem);
        }
    }

    $onDestroy() {
        if (this.unsubscriveSelectItem) { this.unsubscriveSelectItem(); }
    }
};

SidebarController.$inject = ["EventService"];

angular.module("app").controller("SidebarController", SidebarController);
