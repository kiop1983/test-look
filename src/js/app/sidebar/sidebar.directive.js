"use strict";

class SidebarDirective {
    controller = "SidebarController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/sidebar/sidebar.html";
    bindToController = true;
    scope = {};
}

angular.module("app").directive("sidebar", SidebarDirective);
