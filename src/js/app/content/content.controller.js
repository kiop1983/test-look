"use strict";

class ContentController {
    sorting;
    onlyDate;
    filter;
    items = [];
    unsubscribeAddItem;

    constructor(sortingEnum, dataService, eventService) {
        this.sortingEnum = sortingEnum;
        this.dataService = dataService;
        this.eventService = eventService;
    }

    $onInit() {
        if (!this.sorting) {
            this.sorting = this.sortingEnum.title;
        }

        this.items = this.dataService.makeDefaulData();

        this.unsubscribeAddItem = this.eventService.subscribeAddItem((item) => this.items.push(item));
    }

    $onDestroy() {
        if (this.unsubscribeAddItem) { this.unsubscribeAddItem(); }
    }
};

ContentController.$inject = ["sortingEnum", "DataService", "EventService"];

angular.module("app").controller("ContentController", ContentController);

