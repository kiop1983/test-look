"use strict";

class ContentDirective {
    controller = "ContentController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/content/content.html";
    bindToController = true;
    scope = {};
}

angular.module("app").directive("content", ContentDirective);
