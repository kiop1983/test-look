"use strict";

class AppDirective {
    restrict = "E";
    templateUrl = "./js/app/app/app.html";
    //bindToController = true;
    scope = {};
}

angular.module("app").directive("app", AppDirective);
