"use strict";

class ElementsController {
    model = {
        width: 300
    };

    constructor($scope, $element) {
        this.$scope = $scope;
        this.$element = $element;
    }

    setWidth() {
        let width = this.model.width;
        if (!width) {
            width = 1;
            this.model.width = width;
        }
        const root = this.$element[0].querySelector('.elements')
        root.style.width = `${width}px`;
    }

    $onInit() {
        this.setWidth();
    }
};

ElementsController.$inject = ["$scope", "$element"];

angular.module("app")
    .controller("ElementsController", ElementsController);
