"use strict";

class ElementsDirective {
    controller = "ElementsController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/elements/elements.html";
    bindToController = true;
    scope = {};
};

angular.module("app").directive("elements", ElementsDirective);
