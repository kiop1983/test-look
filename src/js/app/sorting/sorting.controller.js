"use strict";

class SortingController {
    sorting;

    constructor(sortingEnum) {
        this.sortingEnum = sortingEnum;
    }

    $onInit() {
        if(!this.sorting) {
            this.sorting = this.sortingEnum.title;
        }
    }
};

SortingController.$inject = ["sortingEnum"];

angular.module("app").controller("SortingController", SortingController);

