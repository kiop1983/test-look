"use strict";

const SortingEnum = {
    date: "date",
    title: "title"
}

class SortingDirective {
    controller = "SortingController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/sorting/sorting.html";
    bindToController = true;
    scope = {
        sorting: "="
    };
}

angular.module("app")
    .directive("sorting", SortingDirective)
    .constant("sortingEnum", SortingEnum);

