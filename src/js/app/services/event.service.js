"use strict";

class EventService {
    subscribers = {};

    selectItemEvent = "selectItemEvent";
    addItemEvent = "addItemEvent";
    changeTagEvent = "changeTagEvent";

    subscribe(eventName, subscriber) {
        if(!this.subscribers[eventName]) { this.subscribers[eventName] = []; }
        this.subscribers[eventName].push(subscriber);
        return () => this.unsubscribe(eventName, subscriber);
    }
    unsubscribe(eventName, subscriber) {
        if(!this.subscribers[eventName]) { return; };
        const index = this.subscribers[eventName].indexOf(subscriber);
        if(index === -1) { return; }
        this.subscribers[eventName] = this.subscribers[eventName].splice(index, 1);
    }
    fire(eventName, data) {
        if(!this.subscribers[eventName]) { return; };
        for(let i = 0; i < this.subscribers[eventName].length; i++) {
            this.subscribers[eventName][i](data);
        }
    }

    subscribeSelectItem(subscriber) { return this.subscribe(this.selectItemEvent, subscriber); }
    fireSelectItem(selectedItem) { this.fire(this.selectItemEvent, selectedItem); }
    
    subscribeAddItem(subscriber) { return this.subscribe(this.addItemEvent, subscriber); }
    fireAddItem(addedItem) { this.fire(this.addItemEvent, addedItem); }

    subscribeChangeTag(subscriber) { return this.subscribe(this.changeTagEvent, subscriber); }
    fireChangeTag(changedItem) { this.fire(this.changeTagEvent, changedItem); }
}

angular.module("app").service("EventService", EventService);
