"use strict";

class SummaryController {
    _items;
    get items() { return this._items; }
    set items(val) {
        this._items = val;
        this.itemsChanges();
    }

    lastItem;
    tags = [];

    unsubscribeAddItem;
    unsubscribeChangeTag;

    constructor(eventService) {
        this.eventService = eventService;
    }

    itemsChanges() {
        this.tags = [];

        if (!this.items || !this.items.length) {
            this.lastItems = null;
            return;
        }

        this.lastItem = this.items.slice().sort((a, b) => b.date - a.date)[0];

        for (let i = 0; i < this.items.length; i++) {
            if (!this.items[i].tags) { continue; }
            for (let j = 0; j < this.items[i].tags.length; j++) {
                const lower = this.items[i].tags[j].toLowerCase();
                if (this.tags.includes(lower)) { continue; }
                this.tags.push(lower);
            }
        }
    }

    $onInit() {
        if (!this.items) {
            this.items = []
        }

        this.unsubscribeAddItem = this.eventService.subscribeAddItem(() => this.itemsChanges());
        this.unsubscribeChangeTag = this.eventService.subscribeChangeTag(() => this.itemsChanges());
        this.unsubscribeAddItem = this.eventService.subscribeAddItem(() => this.itemsChanges());
    }

    $onDestroy() {
        if (this.unsubscribeAddItem) { this.unsubscribeAddItem(); }
        if (this.unsubscribeChangeTag) { this.unsubscribeChangeTag(); }
        if (this.unsubscribeAddItem) { this.unsubscribeAddItem(); }
    }
};

SummaryController.$inject = ["EventService"];

angular.module("app").controller("SummaryController", SummaryController);
