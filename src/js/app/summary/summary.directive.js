"use strict";

class SummaryDirective {
    controller = "SummaryController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/summary/summary.html";
    bindToController = true;
    scope = {
        items: "<"
    };
}

angular.module("app").directive("summary", SummaryDirective);
