"use strict";

class ItemListDirective {
    controller = "ItemListController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/item-list/item-list.html";
    bindToController = true;
    scope = {
        items: "<",
        onlyDate: "<",
        sorting: "<",
        filter: "<"
    };
}

angular.module("app").directive("itemList", ItemListDirective);
