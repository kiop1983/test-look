"use strict";

class ItemListController {
    _items;
    get items() { return this._items; }
    set items(val) {
        this._items = val;
        this.resort();
    }

    _sorting;
    get sorting() { return this._sorting; }
    set sorting(val) {
        this._sorting = val;
        this.resort();
    }

    _filter;
    get filter() { return this._filter; }
    set filter(val) {
        this._filter = val;
        this.resort();
    }

    onlyDate;

    selectedItem;

    displayItems = [];

    unsubscribeAddItem;

    constructor(sortingEnum, eventService) {
        this.sortingEnum = sortingEnum;
        this.eventService = eventService;
    }

    resort() {
        this.displayItems = this.items.slice().sort((a, b) => {
            if (this.sorting === this.sortingEnum.title) {
                return a.title < b.title ? -1 : a.title > b.title ? 1 : 0;
            } else {
                return a.date - b.date;
            }
        });

        if (this.filter) {
            const lower = this.filter.toLowerCase();
            this.displayItems = this.displayItems.filter((a) => a.title.toLowerCase().indexOf(lower) > -1);
        }
    }

    selectItem(item) {
        if (this.selectedItem === item) {
            this.selectedItem = null;
        } else {
            this.selectedItem = item;
        }

        this.eventService.fireSelectItem(this.selectedItem);
    }

    $onInit() {
        if (!this.sorting) {
            this.sorting = this.sortingEnum.title;
        }

        if (!this.items) {
            this.items = [];
        }

        this.unsubscribeAddItem = this.eventService.subscribeAddItem(() => this.resort());
    }

    $onDestroy() {
        if (this.unsubscribeAddItem) { this.unsubscribeAddItem(); }
    }
};

ItemListController.$inject = ["sortingEnum", "EventService"];

angular.module("app").controller("ItemListController", ItemListController);

