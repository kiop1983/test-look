"use strict";

class OnlyDateDirective {
    controller = "OnlyDateController";
    controllerAs = "ctrl";
    restrict = "E";
    templateUrl = "./js/app/only-date/only-date.html";
    bindToController = true;
    scope = {
        onlyDate: "="
    };
}

angular.module("app")
    .directive("onlyDate", OnlyDateDirective);

